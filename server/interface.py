#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

import socket
import ssl

def make_packet(client_id, cluster_id, opcode, data):
    pass

def send(ip, port, packet):
    socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket = ssl.wrap_socket(socket)
    socket.connect(ip, port)
    socket.send(packet)
