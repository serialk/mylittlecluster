sudo rm -rf initrd/cluster/*
sudo cp cluster_bin/* initrd/cluster
cd initrd
find | sudo cpio -o -H newc | gzip -2 > ../core.gz
cd ..
advdef -z4 core.gz
sudo mv core.gz boot
mkdir newiso
sudo mv boot newiso
sudo mkisofs -l -J -R -V TC-cursom -no-emul-boot -boot-load-size 4 \
    -boot-info-table -b boot/isolinux/isolinux.bin \
    -c boot/isolinux/boot.cat -o tinycore.iso newiso
sudo rm -rf initrd
sudo rm -rf boot
sudo rm -rf newiso
sudo cp tinycore.iso /srv/tftpboot/tinycore.iso
