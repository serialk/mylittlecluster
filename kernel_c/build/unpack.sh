if [ -d initrd ]; then
    sudo rm -rf initrd
fi
if [ -d tmp ]; then
    sudo rm -rf tmp
fi
mkdir tmp
mkdir tmp/mnt
sudo mount tinycore.iso tmp/mnt -o loop,ro
sudo cp -a tmp/mnt/boot tmp
sudo mv tmp/boot/core.gz tmp
sudo umount tmp/mnt
sudo rmdir tmp/mnt
mkdir initrd
cd initrd
zcat ../tmp/core.gz | sudo cpio -i -H newc -d
cd ..
sudo rm tmp/core.gz
sudo cp tmp/boot boot -R
sudo rm -rf tmp
