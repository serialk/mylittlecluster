#!/bin/sh

# Variables
sv_ip=$1
sv_port=12113

pr_assign="ASSIGN"
pr_settskhdl="SETTSKHDL"

ps_gettskhdl="GETTSKHDL"
ps_stillalive="STILLALIVE"
ps_gettask="GETTASK"
ps_taskdone="TASKDONE"

path_tskhdl="/cluster/taskhandler"

# Save Task Handler
init_cmd=`echo $ps_gettskhdl | nc $sv_ip $sv_port`
if [ "`echo $init_cmd | grep -Eo "^[A-Z]+"`" = "$pr_settskhdl" ]
then
    if [ -f $path_tskhdl ]
    then
        rm -rf $path_tskhdl
    fi
    echo $init_cmd | tail -c +`echo "$pr_settskhdl#" | wc -m` >> $path_tskhdl
    chmod +x $path_tskhdl

    # Assign Task
    while true
    do
        `$path_tskhdl`
    done
else
    echo Server unreachable
fi
