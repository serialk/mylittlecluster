Usage
=====

./unpack.sh
-----------

This script unpack the tinycore.iso (that should be placed in the same folder)
and create two folders:

* initrd/ that contains the filesystem.
* boot/ that contains the kernel and the bootloader.

./pack.sh
---------

This script pack the initrd, the kernel and the bootloader to build a new
bootable tinycore.iso.


The folder bin/ is copied to initrd/cluster/ to include the new binaries in the
kernel.


After that, it copy tinycore.iso in the /srv/tftpboot/ directory

./repack.sh
-----------

This command is an ./unpack.sh followed by a ./pack.sh


It build a new kernel with up to date binaries (contained in bin/ folder).
