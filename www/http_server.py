#!/usr/bin/env python2

from flask import Flask, render_template, redirect, url_for
import sys
import os
import subprocess
app = Flask(__name__)

WWW_PATH = sys.path[0]
MLC_PATH = os.path.join(WWW_PATH, '..')
MACS_PATH = os.path.join(MLC_PATH, 'macs')
SCRIPTS_PATH = MLC_PATH


def get_file(filename):
    d = {}
    if not os.path.exists(filename):
        open(filename, 'w').close()
    f = open(filename, 'r+')
    for item in f:
        name, mac = item.split(':', 1)
        d[name.strip()] = mac.strip()
    return d


def save_file(filename, d):
    l = ['{0}: {1}'.format(n, m) for n, m in d.items()]
    l.sort()
    c = '\n'.join(l) + '\n'
    open(filename, 'w+').write(c)


def list_sms():
    sms = []
    files = os.path.os.listdir(MACS_PATH)
    for f in files:
        sm, _ = os.path.splitext(f)
        sms.append(int(sm))
    return sorted(sms)


def get_machines(filename):
    posts = []
    f = open(filename, 'r')
    for item in f:
        name, _ = item.split(':', 1)
        posts.append(name.strip())
    return posts


@app.route('/macs/<int:salle>/<name>/<mac>/')
def register(salle, name, mac):
    d = get_file(os.path.join(MACS_PATH, '{0}.txt'.format(salle)))
    d[name] = mac
    save_file(os.path.join(MACS_PATH, '{0}.txt'.format(salle)), d)
    return '[{0}] {1} ({2}) successfuly registered.'.format(salle, name, mac)


@app.route('/')
def wake_index():
    sms = list_sms()
    machines = []
    for sm in sms:
        m = get_machines(os.path.join(MACS_PATH, '{0}.txt'.format(sm)))
        machines.append((sm, m))
    machines = sorted(machines, key=lambda x: x[0])
    return render_template('wake.html', sms=sms, machines=machines)


@app.route('/map')
def wake_map():
    sms = list_sms()
    machines = []
    for sm in sms:
        m = get_machines(os.path.join(MACS_PATH, '{0}.txt'.format(sm)))
        machines.append((sm, m))
    machines = sorted(machines, key=lambda x: x[0])
    return render_template('map.html', sms=sms, machines=machines)


@app.route('/wake_vj')
def wake_vj():
    script = os.path.join(SCRIPTS_PATH, 'wake_sm.sh')
    sms = map(str, list_sms())
    subprocess.Popen([script] + sms, cwd=MLC_PATH)
    return redirect(url_for('wake_index'))


@app.route('/wake_sm/<int:sm>')
def wake_sm(sm):
    script = os.path.join(SCRIPTS_PATH, 'wake_sm.sh')
    subprocess.Popen([script, str(sm)], cwd=MLC_PATH)
    return redirect(url_for('wake_index'))


@app.route('/wake_machine/<int:sm>/<machine>')
def wake_machine(sm, machine):
    script = os.path.join(SCRIPTS_PATH, 'wake_machine.sh')
    subprocess.Popen([script, '{0}:{1}'.format(sm, machine)], cwd=MLC_PATH)
    return redirect(url_for('wake_index'))


@app.route('/wake_rangee/<int:sm>/<int:rangee>')
def wake_rangee(sm, rangee):
    script = os.path.join(SCRIPTS_PATH, 'wake_rangee.sh')
    subprocess.Popen([script, '%s:r%02dp' % (sm, rangee)], cwd=MLC_PATH)
    return redirect(url_for('wake_index'))


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8042, debug=True)
